package main

import "testing"

func verificaMensagem(t *testing.T, resultado, esperado string) {
	t.Helper()
	if resultado != esperado {
		t.Errorf("resultado '%s', esperado '%s'", resultado, esperado)
	}
}

func verificaValor(t *testing.T, resultado, esperado float64) {
	t.Helper()
	if resultado != esperado {
		t.Errorf("resultado '%f', esperado '%f'", resultado, esperado)
	}
}

func TestExibeMenu(t *testing.T) {
	resultado := exibeMenu()
	esperado := "1 - Carnes\n2 - Vegano\n3 - Massas\n0 - Para finalizar pedido"
	verificaMensagem(t, resultado, esperado)
}

func TestBoasVindasCliente(t *testing.T) {
	resultado := boasVindasCliente("Geovana")
	esperado := "Bem Vindo(a) Geovana"
	verificaMensagem(t, resultado, esperado)
}

func TestExibeSubMenuCarnes(t *testing.T) {
	resultado := exibeSubMenu(1)
	esperado := "1a - Parmegiana - R$ 25,00\n1b - Acebolado - R$ 19,99"
	verificaMensagem(t, resultado, esperado)
}

func TestExibeSubMenuVegano(t *testing.T) {
	resultado := exibeSubMenu(2)
	esperado := "2a - Falafel - R$ 10,00"
	verificaMensagem(t, resultado, esperado)
}

func TestExibeSubMenuMassas(t *testing.T) {
	resultado := exibeSubMenu(3)
	esperado := "3a - Bolonhesa - R$ 25,00\n3b - Carbonara - R$ 28,00\n3c - Sugo - R$ 18,00"
	verificaMensagem(t, resultado, esperado)
}

func TestExibeSubMenuInvalido(t *testing.T) {
	resultado := exibeSubMenu(4)
	esperado := "Opção inválida"
	verificaMensagem(t, resultado, esperado)
}

func TestGeraBillingRefeicao1(t *testing.T) {
	resultado := geraBilling([]string{"2a", "3c"})
	esperado := 28.00
	verificaValor(t, resultado, esperado)
}

func TestGeraBillingRefeicao2(t *testing.T) {
	resultado := geraBilling([]string{"1a", "3b"})
	esperado := 53.00
	verificaValor(t, resultado, esperado)
}
