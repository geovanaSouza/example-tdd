# Example TDD

### Pré-requisitos

* Golang

### Build

* go build

### Build e execução

* go run example-tdd.go

### Tests

* go test

### Artefatos gerado no build

* Menu --> Pipelines --> Escolher o build desejado --> Artifacts

## Trabalho Entregue 

### Parabéns 

![Resultados dos testes](ok.png)