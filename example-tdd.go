package main

import (
	"fmt"
	"strings"
)

func exibeMenu() string {
	var menu []string
	menu = append(menu, "1 - Carnes")
	menu = append(menu, "2 - Vegano")
	menu = append(menu, "3 - Massas")
	menu = append(menu, "0 - Para finalizar pedido")
	return strings.Join(menu, "\n")
}

func exibeSubMenu(opcao int) string {
	var menu []string

	switch opcao {
	case 1:
		menu = append(menu, "1a - Parmegiana - R$ 25,00")
		menu = append(menu, "1b - Acebolado - R$ 19,99")
	case 2:
		menu = append(menu, "2a - Falafel - R$ 10,00")
	case 3:
		menu = append(menu, "3a - Bolonhesa - R$ 25,00")
		menu = append(menu, "3b - Carbonara - R$ 28,00")
		menu = append(menu, "3c - Sugo - R$ 18,00")
	default:
		return ("Opção inválida")
	}
	return strings.Join(menu, "\n")
}

func boasVindasCliente(cliente string) string {
	return "Bem Vindo(a) " + cliente
}

func geraBilling(itens []string) float64 {
	var total float64
	/* itensBilling := strings.Split(itens, ",") */

	for _, item := range itens {
		switch item {
		case "1a":
			total += 25.00
		case "1b":
			total += 19.99
		case "2a":
			total += 10.00
		case "3a":
			total += 25.00
		case "3b":
			total += 28.00
		case "3c":
			total += 18.00
		}
	}
	return total
}

func main() {
	var opcao int
	var prato []string
	var itemPrato string
	fmt.Println(boasVindasCliente("Geovana\n"))

	for {
		fmt.Println("Escolha uma das opções do cardápio abaixo")
		fmt.Println(exibeMenu())
		fmt.Scan(&opcao)
		if opcao == 0 {
			fmt.Println("\nA conta ficou R$ ", geraBilling(prato))
			break
		}
		fmt.Println("")
		fmt.Println(exibeSubMenu(opcao))
		fmt.Println("Digite opção escolhida, utilizando vírgula entre as ooções. Ex.: 2a,3c:")
		fmt.Scan(&itemPrato)
		prato = append(prato, itemPrato)
	}
}
